// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

//!+test
package word

import "testing"

// This package is designed to fail

//!-test

// Note: a Palindrome is a word which reads the same backwards or forwards

// The tests below are expected to fail.
// See package gopl.io/ch11/word2 for the fix.
// The Go Programming Language, Alan A. A. Donovan & Brian W. Kernighan

//!+more
func TestFrenchPalindrome(t *testing.T) {
	if !IsPalindrome("été") {
		t.Error(`IsPalindrome("été") = false`)
	}
}

func TestCanalPalindrome(t *testing.T) {
	input := "A man, a plan, a canal: Panama"
	if !IsPalindrome(input) {
		t.Errorf(`IsPalindrome(%q) = false`, input)
	}
}

//!-more
