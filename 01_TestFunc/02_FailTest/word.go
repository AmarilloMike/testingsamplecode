// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See package gopl.io/ch11/word .
// The Go Programming Language, Alan A. A. Donovan & Brian W. Kernighan

// See page 303.

//!+

// Note: a Palindrome is a word which reads the same backwards or forwards

// Package word provides utilities for word games.
package word

// IsPalindrome reports whether s reads the same forward and backward.
// (Our first attempt.)
func IsPalindrome(s string) bool {
	for i := range s {
		if s[i] != s[len(s)-1-i] {
			return false
		}
	}
	return true
}

//!-
